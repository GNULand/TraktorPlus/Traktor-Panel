# Tor Status
> A quite simple Traktor indicator

It's a Python script using AppIndicator, which works on almost all desktop environments (whatever that supports GTK and AppIndicator).

![panel](ScreenShot.png)

Each icon color means something:
* Yellow: Tor is trying to establish a connection.
* Black: Tor service is stopped/Internet connection is missing.
* Purple: Tor has successfully established a connection.

**Please note** this script listens to SOCKS port 9050.

## Installation
1. Install the dependencies with your package manager. For instance, on Ubuntu:
```bash
$ sudo apt install curl
$ sudo apt install gir1.2-appindicator3-0.1 python-appindicator python-gi
```

2. Clone the script repository:
```bash
$ wget https://gitlab.com/GNULand/TraktorPlus/Traktor-Panel/repository/master/archive.zip -O Traktor-Panel.zip
$ unzip Traktor-Panel.zip -d $HOME/Traktor-Panel && cd $HOME/Traktor-Panel/*
```

3. Run the script
```bash
$ chmod +x ./installer-panel.sh
$ ./installer-panel.sh
```

## Contributing
Any merge requests and issues on bug tracker are welcome.

## Changes
[See Changes](https://gitlab.com/GNULand/TraktorPlus/Traktor-Panel/blob/master/CHANGELOG)
