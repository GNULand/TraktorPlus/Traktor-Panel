#!/bin/bash

############################################
#             Traktor Panel V2             #
#  https://gitlab.com/TraktorPlus/Traktor  #
############################################

trap "exit 1" TERM
export TOP_PID=$$


Version="Traktor Panel V2"
#date="2018/03/6 Sat 19:30"
echo -e "$Version\nTraktor Panel will be automatically installed…\n\n"


if [ ! -d "$HOME/.Traktor/" ]; then
    mkdir "$HOME/.Traktor"
fi
if [ ! -d "$HOME/.Traktor/Traktor_Log/" ]; then
    mkdir "$HOME/.Traktor/Traktor_Log"
fi
if [ ! -e "$HOME/.Traktor/Traktor_Log/install.log" ]; then
    echo -e "Traktor $Version Log: [$(date)]" | tee $HOME/.Traktor/Traktor_Log/install.log > /dev/null # echo date and time
fi


# =========/Color/=========

GRE='\033[92m' # Green Light
NC='\033[0m' # White
RD='\033[91m' # Red Light

# =========\Color\=========


function loading {
    spin[0]="-"
    spin[1]="\\"
    spin[2]="|"
    spin[3]="/"
    PID=$!
    echo -n "${spin[0]}"
    while [ -d /proc/$PID ]
    do
        for i in "${spin[@]}"
            do
            echo -ne "\b$i"
            sleep 0.2
        done
    done
    wait $PID
    if [ $? -ne 0 ]; then
        echo -e "\b${RD}Failed!"
        echo -e "\n${NC}Check Traktor Log in $HOME/.Traktor/Traktor_Log/install.log"
        kill -s TERM $TOP_PID
    else
        echo -e "\b${GRE}Done."
    fi
}

function panel {
    rm -rf $HOME/.Traktor/Traktor_Panel
    cp -r Traktor_Panel $HOME/.Traktor/
    rm -rf Traktor_Panel
    chmod +x $HOME/.Traktor/Traktor_Panel/traktor_panel.py
    echo "[Desktop Entry]
Version=1.0
Name=Traktor Panel
Name[fa]=تراکتور پنل
GenericName=Traktor Panel
GenericName[fa]=تراکتور پنل
Comment=Traktor Panel
Comment[fa]=تراکتور پنل
Exec=$HOME/.Traktor/Traktor_Panel/traktor_panel.py
Terminal=false
Type=Application
Categories=Network;Application;
Icon=$HOME/.Traktor/Traktor_Panel/icons/Traktor.png
Keywords=Tor;Browser;Proxy;VPN;Internet;Web" | sudo tee /usr/share/applications/traktor-panel.desktop
}

sudo uname -a &> /dev/null


echo -e "\n\n\n[ #panel ] Install Panel $Version Log: [$(date)]\n" | tee -a $HOME/.Traktor/Traktor_Log/install.log > /dev/null # echo date and time


# Run Panel
echo -ne "${NC}Create Tray Icon...   "
panel >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading

echo -e "\n\nCongratulations, Traktor Panel Successfully Install!!!\n Now, launch Traktor Panel via dash :)." 
