#!/usr/bin/python

####################################################
#                   Traktor V2.3                   #
#                   Traktor Panel                  #
#               Version: 2.0 (beta)                #
# Forked from https://gitlab.com/Ehsaan/tor-status #
####################################################

import os
import signal
import gi
import sys
import commands
import threading
import subprocess

gi.require_version( 'Gtk', '3.0' )
gi.require_version( 'AppIndicator3', '0.1' )
gi.require_version( 'Notify', '0.7' )

from gi.repository import Gtk, Gio, Notify
from gi.repository import AppIndicator3 as AppIndicator
from time import sleep

APPINDICATOR_ID = 'traktor'
Name = "Traktor"

item_status = False
cwd = os.path.abspath( os.path.dirname( __file__ ) )

workerThread = False
scriptRunning = True
Indicator = AppIndicator.Indicator.new( APPINDICATOR_ID, cwd + "/icons/pending.svg", AppIndicator.IndicatorCategory.SYSTEM_SERVICES )

def main( Indicator ):
    Notify.init( APPINDICATOR_ID )
    global workerThread
    Indicator.set_status( AppIndicator.IndicatorStatus.ACTIVE )
    Indicator.set_menu( buildMenu() )

    global item_proxy
    proxy = Gio.Settings.new( "org.gnome.system.proxy" )
    if ( Gio.Settings.get_string( proxy, "mode" ) == "manual" ):
        item_proxy.get_child().set_text( "Disable Proxy" )
    else:
        item_proxy.get_child().set_text( "Enable Proxy" )
    
    workerThread = threading.Thread( target=loop_scan )
    workerThread.daemon = True
    workerThread.start()
    Gtk.main()

def buildMenu():
    menu = Gtk.Menu()
    global item_status
    item_status = Gtk.MenuItem( "Connecting..." )
    item_status.set_sensitive( False )
    
    item_restart = Gtk.MenuItem( "Restart Tor" )
    item_restart.connect( 'activate', TorRestart )

    #item_stop = Gtk.MenuItem( "Stop Tor" )
    #item_stop.connect( 'activate', TorStop )

    global item_proxy
    item_proxy = Gtk.MenuItem( "Toggle Proxy" )
    item_proxy.connect( 'activate', ToggleProxy )

    item_quit = Gtk.MenuItem( "Quit" )
    item_quit.connect( 'activate', quit )

    menu.append( item_status )
    menu.append( Gtk.SeparatorMenuItem() )
    menu.append( item_restart )
    #menu.append( item_stop )
    menu.append( item_proxy )
    menu.append( item_quit )

    menu.show_all()
    return menu

def TorRestart(_):
    return subprocess.call( [ 'pkexec', 'systemctl', 'restart', 'tor.service' ] )

#def TorStop(_):
    #return subprocess.call( [ 'pkexec', 'systemctl', 'stop', 'tor.service'] )

def ToggleProxy(_):
    proxy = Gio.Settings.new( "org.gnome.system.proxy" )
    if ( Gio.Settings.get_string( proxy, "mode" ) == "manual" ):
        proxy.set_string( "mode", "none" )
        Notify.Notification.new( Name, "Proxy is Disabled", None).show()
        item_proxy.get_child().set_text( "Enable Proxy" )
    else:
        proxy.set_string( "mode", "manual" )
        item_proxy.get_child().set_text( "Disable Proxy" )
        Notify.Notification.new( Name, "Proxy is enabled", None).show()

def quit(_):
    Gtk.main_quit()
    scriptRunning = False
    sys.exit(_)

TorConnected = False
def getTorStatus():
    global TorConnected
    output = commands.getoutput( 'ping -c 1 8.8.8.8 | grep "0% packet loss"' )
    
    if output[0:33] != "1 packets transmitted, 1 received":
        Indicator.set_icon( cwd + "/icons/disable.svg" )
        return 'No Internet'

    output = commands.getoutput( 'curl -I --proxy socks5h://127.0.0.1:9050 https://3g2upl4pq6kufc4m.onion' )
    if 'HTTP/2 200' in output:
        Indicator.set_icon( cwd + "/icons/enable.svg" )
        TorConnected = True
        return 'Connected'
    elif 'HTTP/1.1 200' in output:
        Indicator.set_icon( cwd + "/icons/enable.svg" )
        TorConnected = True
        return 'Connected'
    
    TorConnected = False
    Indicator.set_icon( cwd + "/icons/pending.svg" )
    return 'Connecting...'

def loop_scan():
    global TorConnected
    global scriptRunning
    while scriptRunning:
        global item_proxy
        proxy = Gio.Settings.new( "org.gnome.system.proxy" )
        if ( Gio.Settings.get_string( proxy, "mode" ) == "manual" ):
            item_proxy.get_child().set_text( "Disable Proxy" )
        else:
            item_proxy.get_child().set_text( "Enable Proxy" )
        
        output = commands.getoutput( 'ps -A' )
        if 'tor' in output:
            if item_status != False:
                if TorConnected:
                    item_status.get_child().set_text( getTorStatus() )
                else:
                    Indicator.set_icon( cwd + "/icons/pending.svg" )
                    item_status.get_child().set_text( "Connecting..." )
                    item_status.get_child().set_text( getTorStatus() )
        else:
            Indicator.set_icon( cwd + "/icons/disable.svg" )
            if item_status != False:
                TorConnected = False
                item_status.get_child().set_text( 'Tor is stopped' )

        sleep( 5 )

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)

main( Indicator ) 
